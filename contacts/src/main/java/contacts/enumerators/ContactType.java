package contacts.enumerators;

public enum ContactType {
	telephoneNumber,
	emailAddress,
	homeAdress
}
