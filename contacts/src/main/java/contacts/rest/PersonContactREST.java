package contacts.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contacts.domain.PersonContact;
import contacts.services.PersonContactService;
@RestController
@RequestMapping(path="/personContact") 
public class PersonContactREST {
	
	@Autowired 
	private PersonContactService personContactService;
	@PostMapping(path="/add") 
	@ResponseBody
	public String addNewPersonContact (@RequestParam String personId
													,@RequestParam String contacType
													,@RequestParam String contacValue) {
		
		String result = personContactService.addContactToPerson(personId,contacType,contacValue);
		return result;
	}

	@GetMapping(path="/all")
	@ResponseBody
	public Iterable<PersonContact> getAllPersonContact() {
		return personContactService.findAll();
	}
	@DeleteMapping(path="/remove")
	@ResponseBody
	public String removePersonContact(@RequestParam String contactId) {
		String result = personContactService.remove(contactId);
		return result;
	}
	@PutMapping(path="/update")
	@ResponseBody
	public String updatePersonContact(@RequestParam String contactId
																	,@RequestParam String contacType
																	,@RequestParam String contacValue) {
		String result = personContactService.update(contactId,contacType,contacValue);
		return result;
	}
}
