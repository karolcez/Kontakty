package contacts.rest;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import contacts.domain.Person;
import contacts.services.PersonService;


@RestController
@RequestMapping(path="/person")
public class PersonREST {

	@Autowired 
	private PersonService personService;
	@PostMapping(path="/add")
	@ResponseBody
	public String addNewPersonContact (@RequestParam String firstName
			,@RequestParam String lastName
			,@RequestParam String gender
			,@RequestParam String bornDate
			,@RequestParam String personalIdentityNumber) throws ParseException {

		String result = personService.addPerson(firstName,lastName,gender,bornDate,personalIdentityNumber);
		return result;
	}

	@GetMapping(path="/all")
	@ResponseBody
	public Iterable<Person> getAllPerson() {
		return personService.findAll();
	}
	@DeleteMapping(path="/remove")
	@ResponseBody
	public  String removePerson(@RequestParam String id) {
		String result = personService.removeById(id);

		return result;
	}
	@PutMapping(path="/update")
	@ResponseBody
	public String updatePerson(@RequestParam String personId
			,@RequestParam String firstName
			,@RequestParam String lastName
			,@RequestParam String gender
			,@RequestParam String bornDate
			,@RequestParam String personalIdentityNumber) throws ParseException {
		String result = personService.update(personId,firstName,lastName,gender,bornDate,personalIdentityNumber);
		return result;
	}
	
	 

}
