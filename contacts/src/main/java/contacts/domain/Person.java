package contacts.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import contacts.enumerators.Gender;
import lombok.Data;

@Data
@Entity
@Table(name = "person")
public class Person {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@NotNull
 //   @Size(min=2, max=30)
	private String firstName;
	@NotNull
  //  @Size(min=2, max=30)
	private String lastName;   
	private Gender gender;
	private Date bornDate;
	private String personalIdentityNumber;

	@OneToMany(mappedBy="person", cascade = CascadeType.ALL)
	private Set<PersonContact> contacts;

	


}
