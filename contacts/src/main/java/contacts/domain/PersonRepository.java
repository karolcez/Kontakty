package contacts.domain;

import org.springframework.data.repository.CrudRepository;

import contacts.domain.Person;



public interface PersonRepository extends CrudRepository<Person, Long> {

}