package contacts.domain;

import org.springframework.data.repository.CrudRepository;

import contacts.domain.PersonContact;



public interface PersonContactRepository extends CrudRepository<PersonContact, Long> {

}