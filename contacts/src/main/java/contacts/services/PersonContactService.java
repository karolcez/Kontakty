package contacts.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

//import contacts.rest.Autowired;
//import contacts.rest.PersonContact;
import contacts.domain.*;
import contacts.enumerators.*;
import contacts.tools.ServerLoger;

@Service
public class PersonContactService {
	@Autowired 
	private PersonContactRepository personContactRepository;
	@Autowired 
	private PersonRepository personRepository;
	public String addContactToPerson(String personId,String contactType,String contacValue)
	{
		try
		{
		Person person = personRepository.findOne(Long.parseLong(personId));
		if(person != null)
		{
			PersonContact personContact = new PersonContact();
			personContact.setContactType(ContactType.valueOf(contactType));
			personContact.setContactValue(contacValue);
			personContact.setPerson(person);
			person.getContacts().add(personContact);
			personContactRepository.save(personContact);
			personRepository.save(person);
			return "ContactSaved";
		}
		}catch (Exception e) {
			ServerLoger.LogSerwer("Error. Create Person COntact.", e);
			return "Error";
		}

		return "PersonNotFound";
	}
	public Iterable<PersonContact> findAll()
	{
		try
		{
		Iterable<PersonContact> result = personContactRepository.findAll();
		return result;
		}catch (Exception e) {
			ServerLoger.LogSerwer("Error. Find All Person Contact.", e);
			return new ArrayList<PersonContact>();
		}

	}
	public String remove(String contactId)
	{
		try
		{
		if(personContactRepository.exists(Long.parseLong(contactId)))
		{
			personContactRepository.delete(Long.parseLong(contactId));
			return "PersonContactRemoved";
		}
		}catch (Exception e) {
			ServerLoger.LogSerwer("Error. Remove Person Contact.", e);
			return "Error";
		}
		return "PersonContactNotFound";
	}
	public String update(String contactId,String contactType,String contacValue)
	{
		try
		{
		PersonContact personContact = personContactRepository.findOne(Long.parseLong(contactId));
		if(personContact!=null)
		{
			
			personContact.setContactType(ContactType.valueOf(contactType));
			personContact.setContactValue(contacValue);
			
			personContactRepository.save(personContact);
			return "PersonContactUpdated";
		}
		}catch (Exception e) {
			ServerLoger.LogSerwer("Error. Update Person Contact.", e);
			return "Error";
		}
		return "PersonContactNotFound";
	}




}
