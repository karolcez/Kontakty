package contacts.services;



import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import contacts.domain.*;
import contacts.tools.*;
import contacts.enumerators.Gender;;

@Service
public class PersonService {
	@Autowired 
	private PersonRepository personRepository;

	private Person createPerson(String firstName,String lastName,String gender,
			String bornDate,String personalIdentityNumber) throws ParseException
			{
		Person newPerson = new Person();
		try
		{

			newPerson.setFirstName(firstName);
			newPerson.setLastName(lastName);
			newPerson.setGender(Gender.valueOf(gender));

			if(Validators.isDateValid(bornDate))
				newPerson.setBornDate(Parsers.parseStringToDate(bornDate));
			else
				newPerson.setBornDate(new Date());
				
			

			newPerson.setPersonalIdentityNumber(personalIdentityNumber);
			if(newPerson.getContacts()==null)
				newPerson.setContacts(new HashSet<PersonContact>());
		}catch (Exception e) {
			ServerLoger.LogSerwer("Error. Create Person.", e);
			return new Person();
		}
		return newPerson;
			}

	public String addPerson(String firstName,String lastName,String gender,
			String bornDate,String personalIdentityNumber) throws ParseException
			{
		try
		{
			Person newPerson = createPerson(firstName,lastName,gender,bornDate,personalIdentityNumber);
			personRepository.save(newPerson);
		}catch (Exception e) {
			ServerLoger.LogSerwer("Error. Add Person.", e);
			return "Error";
		}
		return "PersonSaved";
			}
	public Iterable<Person> findAll()
	{
		try
		{
		Iterable<Person> result = personRepository.findAll();
		return result;
		}catch (Exception e) {
			ServerLoger.LogSerwer("Error. Find all person entities.", e);
			return new ArrayList<Person>();
		}

	}
	public String remove(String firstName,String lastName,String gender,
			String bornDate,String personalIdentityNumber,String id)
	{
		try
		{
			Person person = createPerson(firstName,lastName,gender,bornDate,personalIdentityNumber);
			person.setId(Long.parseLong(id));
			if(personRepository.exists(Long.parseLong(id)))
			{
				personRepository.delete(person);
			}
		}catch (Exception e) {
			ServerLoger.LogSerwer("Error. Removing Person.", e);
			return "Error";
		}

		return "PersonRemoved";
	}
	public String removeById(String id)
	{
		try
		{
		if(personRepository.exists(Long.parseLong(id)))
		{
			personRepository.delete(Long.parseLong(id));
			return "PersonRemoved";
		}
		}
		catch (Exception e) {
			ServerLoger.LogSerwer("Error. Removing Person by id.", e);
			return "Error";
		}
		return "PersonNotFound";
	}
	public String update(String id, String firstName,String lastName,String gender,
			String bornDate,String personalIdentityNumber) throws ParseException
			{
		try
		{
			Person person = personRepository.findOne(Long.parseLong(id));
			if(person != null)
			{

				person.setFirstName(firstName);
				person.setLastName(lastName);
				person.setGender(Gender.valueOf(gender));

				if(Validators.isDateValid(bornDate))
					person.setBornDate(Parsers.parseStringToDate(bornDate));

				person.setPersonalIdentityNumber(personalIdentityNumber);
				personRepository.save(person);
				return "PersonUpdated";
			}
		}catch (Exception e) {
			ServerLoger.LogSerwer("Error. Updating Person.",e);
			return "Error";
		}
		return "PersonNotFound";
			}
}

