package contacts.tools;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class ServerLoger {

	static final String loggerFilePath = "C:\\Users\\karol\\workspace\\contacts\\src\\main\\resources\\ServerExceptionLogs.txt";
	
	public static void LogSerwer(String message, Exception exception)
	{
		
		Logger logger = Logger.getLogger("ServerLogs");  
		FileHandler fh;  

		try {  

			fh = new FileHandler(loggerFilePath);  
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();  
			fh.setFormatter(formatter);  

			logger.info(message+"\n"+exception+"\n"+exception.getStackTrace());  

		} catch (SecurityException e) {  
			e.printStackTrace();  
		} catch (IOException e) {  
			e.printStackTrace();  
		}  

	}
}
