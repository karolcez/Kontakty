package contacts.tools;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import contacts.AplicationGlobals;

public class Parsers {
	public static Date parseStringToDate(String target) throws ParseException
	{
	    DateFormat df = new SimpleDateFormat(AplicationGlobals.DATE_FORMAT);
	    Date result =  df.parse(target);
		return result;  
	}
}
