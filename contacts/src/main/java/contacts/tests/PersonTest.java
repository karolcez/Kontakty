package contacts.tests;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;


import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import contacts.services.PersonService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
//@WebMvcTest(GreetingController.class)
public class PersonTest {

    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private PersonService personService;

    @Test
    public void addPersonTest() throws Exception {
        this.mockMvc.perform(post("/person/add?firstName=jan&lastName=kowal&gender=male&bornDate=kowal&personalIdentityNumber=kowal")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("PersonSaved")));
    }
    @Test
    public void findAllPersonTest() throws Exception {
        this.mockMvc.perform(get("/person/all")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));
    }
    @Test
    public void updatePersonTest() throws Exception {
    	personService.addPerson("firstName", "lastName", "male", "12-12-1987", "9817234");
        this.mockMvc.perform(put("/person/update?personId=2&firstName=jan&lastName=kowal&gender=male&bornDate=kowal&personalIdentityNumber=kowal")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("PersonUpdated")));
    }
    @Test
    public void updateNotFoundPersonTest() throws Exception {
        this.mockMvc.perform(put("/person/update?personId=1000&firstName=janinka&lastName=kowal&gender=male&bornDate=kowal&personalIdentityNumber=kowal"))
        .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("PersonNotFound")));
    }
    @Test
    public void deletePersonTest() throws Exception {
    	personService.addPerson("firstName", "lastName", "male", "12-12-1987", "9817234");
        this.mockMvc.perform(delete("/person/remove?id=1")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("PersonRemoved")));
    }
    @Test
    public void deleteNotFoundPersonTest() throws Exception {
        this.mockMvc.perform(delete("/person/remove?id=1000")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("PersonNotFound")));
    }
  
}