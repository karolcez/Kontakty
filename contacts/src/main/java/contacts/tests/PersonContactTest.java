package contacts.tests;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import contacts.services.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PersonContactTest {

    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private PersonService personService;
    @Autowired
    private PersonContactService personContactService;
    

    @Test
    public void addPersonContactTest() throws Exception {
    	personService.addPerson("firstName", "lastName", "male", "12-12-1987", "9817234");
        this.mockMvc.perform(post("/personContact/add?personId=1&contacType=emailAddress&contacValue=karsdfolcez@gsafzesta.pl")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("ContactSaved")));
    }
    @Test
    public void findAllPersonContactTest() throws Exception {
        this.mockMvc.perform(get("/personContact/all")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));
    }
    @Test
    public void deletePersonContactTest() throws Exception {
    	personService.addPerson("firstName", "lastName", "male", "12-12-1987", "9817234");    	
    	personContactService.addContactToPerson("1", "emailAddress", "lok@asdf.pl");
        this.mockMvc.perform(delete("/personContact/remove?contactId=1")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("PersonContactRemoved")));
    }
    @Test
    public void deleteNotFoundPersonContactTest() throws Exception {
        this.mockMvc.perform(delete("/personContact/remove?contactId=1000")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("PersonContactNotFound")));
    }
    @Test
    public void updatePersonContactTest() throws Exception {
    	personService.addPerson("firstName", "lastName", "male", "12-12-1987", "9817234");
    	personContactService.addContactToPerson("1", "emailAddress", "lok@asdf.pl");

        this.mockMvc.perform(put("/personContact/update?contactId=1&contacType=emailAddress&contacValue=email@asdf.mol")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("PersonContactUpdated")));
    }
    @Test
    public void updateNotFoundPersonContactTest() throws Exception {
        this.mockMvc.perform(put("/personContact/update?contactId=1000&contacType=emailAddress&contacValue=email@asdf.mol")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("PersonContactNotFound")));
    }
}